## qb_device (noetic) - 3.1.0-1

The packages in the `qb_device` repository were released into the `noetic` distro by running `/usr/local/bin/bloom-release qb_device --track noetic --rosdistro noetic --edit` on `Wed, 26 Jul 2023 13:07:27 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_gazebo`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `3.0.5-1`
- old version: `3.0.5-1`
- new version: `3.1.0-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_device (melodic) - 3.0.5-2

The packages in the `qb_device` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_device --track melodic --rosdistro melodic` on `Mon, 09 Jan 2023 09:47:46 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_gazebo`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `2.0.1-0`
- old version: `3.0.5-1`
- new version: `3.0.5-2`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_device (noetic) - 3.0.5-1

The packages in the `qb_device` repository were released into the `noetic` distro by running `/usr/local/bin/bloom-release qb_device --track noetic --rosdistro noetic` on `Wed, 07 Sep 2022 15:25:47 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_gazebo`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `3.0.4-1`
- old version: `3.0.4-1`
- new version: `3.0.5-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_device (melodic) - 3.0.5-1

The packages in the `qb_device` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_device --track melodic --rosdistro melodic` on `Wed, 07 Sep 2022 15:19:00 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_gazebo`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `2.0.1-0`
- old version: `3.0.4-3`
- new version: `3.0.5-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_device (noetic) - 3.0.4-1

The packages in the `qb_device` repository were released into the `noetic` distro by running `/usr/local/bin/bloom-release qb_device --track noetic --rosdistro noetic --new-track` on `Mon, 18 Jul 2022 11:58:09 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_gazebo`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `2.2.2-1`
- new version: `3.0.4-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_device (melodic) - 3.0.4-3

The packages in the `qb_device` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_device --track melodic --rosdistro melodic` on `Mon, 18 Jul 2022 11:48:23 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_gazebo`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `2.0.1-0`
- old version: `3.0.4-2`
- new version: `3.0.4-3`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_device (melodic) - 3.0.4-2

The packages in the `qb_device` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_device --track melodic --rosdistro melodic` on `Mon, 18 Jul 2022 11:42:03 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_gazebo`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `2.0.1-0`
- old version: `3.0.4-1`
- new version: `3.0.4-2`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_device (melodic) - 3.0.4-1

The packages in the `qb_device` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_device --track melodic --rosdistro melodic` on `Mon, 18 Jul 2022 11:34:13 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_gazebo`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `2.0.1-0`
- old version: `2.2.1-3`
- new version: `3.0.4-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_device (noetic) - 2.2.2-1

The packages in the `qb_device` repository were released into the `noetic` distro by running `/usr/local/bin/bloom-release qb_device --track noetic --rosdistro noetic` on `Wed, 01 Sep 2021 13:37:04 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_gazebo`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `2.2.1-1`
- old version: `2.2.1-1`
- new version: `2.2.2-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_device (noetic) - 2.2.1-1

The packages in the `qb_device` repository were released into the `noetic` distro by running `/usr/local/bin/bloom-release qb_device --track noetic --rosdistro noetic --new-track` on `Fri, 27 Aug 2021 14:33:42 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_gazebo`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.2.1-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_device (kinetic) - 2.2.1-1

The packages in the `qb_device` repository were released into the `kinetic` distro by running `/usr/local/bin/bloom-release qb_device --track kinetic --rosdistro kinetic` on `Fri, 27 Aug 2021 14:25:27 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_gazebo`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `2.1.1-1`
- old version: `2.1.1-1`
- new version: `2.2.1-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_device (melodic) - 2.2.1-3

The packages in the `qb_device` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_device --track melodic --rosdistro melodic` on `Fri, 27 Aug 2021 14:17:26 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_gazebo`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `2.0.1-0`
- old version: `2.2.1-2`
- new version: `2.2.1-3`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_device (melodic) - 2.2.1-2

The packages in the `qb_device` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_device --track melodic --rosdistro melodic` on `Fri, 27 Aug 2021 14:14:16 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_gazebo`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `2.0.1-0`
- old version: `2.2.1-1`
- new version: `2.2.1-2`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_device (melodic) - 2.2.1-1

The packages in the `qb_device` repository were released into the `melodic` distro by running `/usr/local/bin/bloom-release qb_device --track melodic --rosdistro melodic` on `Fri, 27 Aug 2021 14:02:37 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_gazebo`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `2.0.1-0`
- old version: `2.0.1-0`
- new version: `2.2.1-1`

Versions of tools used:

- bloom version: `0.10.7`
- catkin_pkg version: `0.4.23`
- rosdep version: `0.21.0`
- rosdistro version: `0.8.3`
- vcstools version: `0.1.42`


## qb_device (kinetic) - 2.1.1-1

The packages in the `qb_device` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release qb_device --track kinetic --rosdistro kinetic` on `Mon, 07 Oct 2019 10:25:58 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `2.1.0-1`
- old version: `2.1.0-1`
- new version: `2.1.1-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.13`
- rosdep version: `0.16.1`
- rosdistro version: `0.7.4`
- vcstools version: `0.1.42`


## qb_device (kinetic) - 2.1.0-1

The packages in the `qb_device` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release qb_device --track kinetic --rosdistro kinetic` on `Tue, 28 May 2019 15:58:27 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `2.0.1-0`
- old version: `2.0.1-0`
- new version: `2.1.0-1`

Versions of tools used:

- bloom version: `0.8.0`
- catkin_pkg version: `0.4.12`
- rosdep version: `0.15.2`
- rosdistro version: `0.7.4`
- vcstools version: `0.1.40`


## qb_device (melodic) - 2.0.1-0

The packages in the `qb_device` repository were released into the `melodic` distro by running `/usr/bin/bloom-release qb_device --track melodic --rosdistro melodic` on `Fri, 01 Jun 2018 09:38:17 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `2.0.0-0`
- new version: `2.0.1-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_device (lunar) - 2.0.1-0

The packages in the `qb_device` repository were released into the `lunar` distro by running `/usr/bin/bloom-release qb_device --track lunar --rosdistro lunar` on `Fri, 01 Jun 2018 09:32:13 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `2.0.0-0`
- old version: `2.0.0-0`
- new version: `2.0.1-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_device (kinetic) - 2.0.1-0

The packages in the `qb_device` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release qb_device --track kinetic --rosdistro kinetic` on `Fri, 01 Jun 2018 09:25:50 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `2.0.0-0`
- old version: `2.0.0-0`
- new version: `2.0.1-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_device (melodic) - 2.0.0-0

The packages in the `qb_device` repository were released into the `melodic` distro by running `/usr/bin/bloom-release qb_device --track melodic --rosdistro melodic --new-track` on `Thu, 31 May 2018 15:20:36 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.0.0-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_device (lunar) - 2.0.0-0

The packages in the `qb_device` repository were released into the `lunar` distro by running `/usr/bin/bloom-release qb_device --track lunar --rosdistro lunar --new-track` on `Thu, 31 May 2018 15:03:15 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `2.0.0-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_device (kinetic) - 2.0.0-0

The packages in the `qb_device` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release qb_device --track kinetic --rosdistro kinetic` on `Thu, 31 May 2018 10:20:22 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`
- `qb_device_utils`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `1.2.2-0`
- old version: `1.2.2-0`
- new version: `2.0.0-0`

Versions of tools used:

- bloom version: `0.6.4`
- catkin_pkg version: `0.4.2`
- rosdep version: `0.12.2`
- rosdistro version: `0.6.8`
- vcstools version: `0.1.40`


## qb_device (kinetic) - 1.2.2-0

The packages in the `qb_device` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_device` on `Thu, 30 Nov 2017 09:14:58 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `1.1.0-0`
- old version: `1.1.0-0`
- new version: `1.2.2-0`

Versions of tools used:

- bloom version: `0.6.1`
- catkin_pkg version: `0.3.9`
- rosdep version: `0.11.8`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## qb_device (kinetic) - 1.1.0-0

The packages in the `qb_device` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_device` on `Fri, 24 Nov 2017 11:57:58 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `1.0.8-0`
- old version: `1.0.8-0`
- new version: `1.1.0-0`

Versions of tools used:

- bloom version: `0.6.1`
- catkin_pkg version: `0.3.9`
- rosdep version: `0.11.8`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## qb_device (kinetic) - 1.0.8-0

The packages in the `qb_device` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_device` on `Tue, 27 Jun 2017 13:01:33 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `1.0.7-0`
- old version: `1.0.7-0`
- new version: `1.0.8-0`

Versions of tools used:

- bloom version: `0.5.26`
- catkin_pkg version: `0.3.5`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## qb_device (kinetic) - 1.0.7-0

The packages in the `qb_device` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_device` on `Mon, 26 Jun 2017 08:51:26 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `1.0.6-0`
- old version: `1.0.6-0`
- new version: `1.0.7-0`

Versions of tools used:

- bloom version: `0.5.26`
- catkin_pkg version: `0.3.5`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## qb_device (kinetic) - 1.0.6-0

The packages in the `qb_device` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_device` on `Fri, 23 Jun 2017 15:53:20 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: https://bitbucket.org/qbrobotics/qbdevice-ros-release.git
- rosdistro version: `1.0.1-0`
- old version: `1.0.1-0`
- new version: `1.0.6-0`

Versions of tools used:

- bloom version: `0.5.26`
- catkin_pkg version: `0.3.5`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


## qb_device (kinetic) - 1.0.1-0

The packages in the `qb_device` repository were released into the `kinetic` distro by running `/usr/bin/bloom-release --rosdistro kinetic --track kinetic qb_device --edit` on `Mon, 19 Jun 2017 13:01:41 -0000`

These packages were released:
- `qb_device`
- `qb_device_bringup`
- `qb_device_control`
- `qb_device_description`
- `qb_device_driver`
- `qb_device_hardware_interface`
- `qb_device_msgs`
- `qb_device_srvs`

Version of package(s) in repository `qb_device`:

- upstream repository: https://bitbucket.org/qbrobotics/qbdevice-ros.git
- release repository: unknown
- rosdistro version: `null`
- old version: `null`
- new version: `1.0.1-0`

Versions of tools used:

- bloom version: `0.5.26`
- catkin_pkg version: `0.3.5`
- rosdep version: `0.11.5`
- rosdistro version: `0.6.2`
- vcstools version: `0.1.39`


